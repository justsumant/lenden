const Collection = require('../models/collection.model');
const Customer = require('../models/customer.model');
const Log = require('../models/log.model');
let logMessage='';


/**
 * get all the existing collections
*/
exports.getCollections = (req, res, next) => {
    const limit = parseInt(req.params.limit);
    const offset = parseInt(req.params.offset);

    Collection.findAll({
        include: [{
            model: Customer,
        }],
        order: [
          ['createdAt', 'DESC']
        ],
        offset: offset,
        limit: limit
    }
        ).then(collections => {
        return res.json(collections);
    }).catch(err => {
        console.log(err);
    });
};


exports.addCollection = (req, res, next) => {

    const amount = req.body.amount;
    const submittedBy = req.body.submittedBy;
    const date = req.body.date;
    const note = req.body.note;
    const customerId = req.body.customerId;
    const dueId = req.body.dueId;
    const interestRate= req.body.interestRate;
    const newCollection = {
        amount: amount,
        submittedBy: submittedBy,
        date: date,
        note: note,
        customerId: customerId,
        interestRate: interestRate,
        dueId: dueId
    }

    Collection.create(newCollection).then(collection => {

        logMessage = '<h5>New Collection added </h5>' + JSON.stringify(newCollection);
            Log.create({
                eventName: logMessage,
                customerId: customerId
            }).then(result=>{
                console.log('log created');
            }).catch(err=>{
                console.log('error while creating log', err);
            });

        return res.json(collection);
    }).catch(err => {
        console.log(err);
        throw new Error(err);
    })

}

exports.removeCollection = (req, res, next) => {
    const collectionId = req.params.collectionId;
    Collection.findOne({
        where: {
            id: collectionId
        }
    }).then(result => {
        if(result){
            logMessage = '<h5>Collection deleted </h5>' + JSON.stringify(result);
            Log.create({
                eventName: logMessage,
                customerId: result.customerId
            }).then(result=>{
                console.log('log created');
            }).catch(err=>{
                console.log('error while creating log', err);
            });
            return result.destroy();
        }
        
    }).then(result=>{
        console.log('collection deleted successfully');
        res.json(result);
    }).catch(err => {
        console.log(err);
        throw new Error(err);
    })
}

exports.updateCollection = (req, res, next) => {
    const amount = req.body.amount;
    const submittedBy = req.body.submittedBy;
    const date = req.body.date;
    const note = req.body.note;
    const customerId = req.body.customerId;
    const collectionId = req.body.id;
    const dueId = req.body.dueId;
    const interestRate = req.body.interestRate;
    const updatedCollection={
        amount: amount,
        submittedBy: submittedBy,
        date: date,
        note: note,
        interestRate: interestRate,
        customerId: customerId,
        dueId: dueId
    };
    Collection.update(updatedCollection, {
        where: {
            id: collectionId
        }
    }).then(collection => {
        logMessage = '<h5>Collection updated to </h5>' + JSON.stringify(updatedCollection);
            Log.create({
                eventName: logMessage,
                customerId: customerId
            }).then(result=>{
                console.log('log created');
            }).catch(err=>{
                console.log('error while creating log', err);
            });

        return res.json(collection);
    }).catch(err => {
        console.log(err);
        throw new Error(err);
    })
}

exports.collectionsOfCustomer = (req,res,next)=>{
    const customerId = req.params.customerId;
    Collection.findAll({where: {
        customerId: customerId
    }}).then(collections=>{
        res.json(collections);
    }).catch(err=>{
        console.log(err);
        throw new Error(err);
    })
}

exports.getCollectionsCount = (req, res, next) => {
    Collection.count().then(count => {
        res.json(count);
    }).catch(err => {
        console.log(err);
        throw new Error(err);
    });
}