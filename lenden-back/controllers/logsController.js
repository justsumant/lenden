const Log = require("../models/log.model")
const { Op } = require("sequelize");


exports.getAllLogs = (req, res, next) => {
    const fromDate = req.params.fromDate.trim() || '2000-10-26';
    const toDate = req.params.toDate.trim() || '2200-10-26';
    const limitStart = parseInt(req.params.limitStart);
    const limitSize = parseInt(req.params.limitSize);

    console.log('testing logss...', fromDate, toDate, limitStart, limitSize);
    Log.findAll({
        where: {
            // [Op.and]:[
            //     {
            //         createdAt: {
            //             [Op.gt]: fromDate
            //           }
            //     },
            //     {
            //         createdAt: {
            //             [Op.lt]: toDate
            //           }
            //     }
            // ]
            createdAt: {
                [Op.between]: [fromDate, toDate],
            }
        },
        // attributes: {
        //     include: [
        //       [sequelize.fn('COUNT', sequelize.col('hats')), 'n_hats']
        //     ]
        //   },
        order: [
            ['createdAt', 'DESC']
        ],
        offset: limitStart,
        limit: limitSize

    }).then(result => {
        res.json(result);
    }).catch(err => {
        console.log(err);
        throw new Error(err);
    });
}

exports.getLogSize = (req, res, next) => {
    const fromDate = req.params.fromDate.trim() || '2000-10-26';
    const toDate = req.params.toDate.trim() || '2200-10-26';
    Log.count(
        {
            where: {
                createdAt: {
                    [Op.between]: [fromDate, toDate],
                }
            }
        }).then(size => {
            res.json(size);
        }).catch(err => {
            console.log(err);
            throw new Error(err);
        });
}


exports.getCustomersLogs = (req, res, next) => {
    const customerId = req.body.customerId;
    Log.findAll({
        where: {
            customerId: customerId
        }
    }).then(result => {
        res.json(result);
    }).catch(err => {
        console.log(err);
        throw new Error(err);
    })
}