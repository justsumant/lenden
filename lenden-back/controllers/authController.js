const crypto= require('crypto');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const nodemailer= require('nodemailer');
const sendgridTransport = require('nodemailer-sendgrid-transport');
const { Op } = require("sequelize");
const dotenv = require('dotenv');
dotenv.config();


const mailFrom=process.env.MAIL_FROM;
const mailAccountCreatedSubject=process.env.MAIL_ACCOUNT_CREATED_SUBJECT;
const mailAccountResetTokenSubject=process.env.MAIL_ACCOUNT_RESET_TOKEN_SUBJECT;
const mailAccountCreatedHTML=process.env.MAIL_ACCOUNT_CREATED_HTML;

const appUrl = process.env.APP_URL;
const tokenSecret=process.env.TOKEN_SECRET;
const tokenExpirationDuration=process.env.TOKEN_EXPIRATION_DURATION;

const transporter = nodemailer.createTransport(sendgridTransport({
    auth: {
        api_key:process.env.API_KEY
    }
}));

/**
 * login the admin
*/
exports.login = (req, res, next) => {
    const email = req.body.userName;
    const password = req.body.password;
    let loadedUser;
    // find the one user with reecived username
    User.findOne({where:{email: email}}).then(user => {
        if(!user){
            const error = new Error('User cannot be found with given email ID.');
            error.statusCode = 401;
            throw(error);
        }
        loadedUser = user;
        bcrypt.compare(password, user.password)
        .then(doMatch=>{
            if(!doMatch){
                const error = new Error('Wrong Password');
                error.statusCode = 401;
                throw(error);
            }
            const token = jwt.sign(
                {email: loadedUser.email, id: loadedUser.id}, 
                tokenSecret, 
                {expiresIn: tokenExpirationDuration});
                res.status(200).json({token: token, loadedUserId: loadedUser.id.toString()})
        }).catch(err=>{
            if(err.statusCode){
                err.statusCode=500;
            }
            console.log(err);
            res.status(401).json(err);
        });
        
    }).catch(err => {
        console.log(err);
        res.status(401).json(err);
    });
};

/**
 * sign up with received informations
*/
exports.signup=(req,res,next)=>{
    let name=req.body.name;
    let password = req.body.password;
    let email = req.body.email;

    User.findOne({ where: { email: email } })
        .then(user => {
            if (user) {
                console.log('user already exists, skipping creation of new user.');
                res.status=409;
                throw new Error('user already exists');
            }
            bcrypt.hash(password, 12).then(hashedPassword => {
                User.create({
                    name: name,
                    email: email,
                    password: hashedPassword
                }).then(user=>{
                    res.json(user);
                    transporter.sendMail({
                        to:email,
                        from:mailFrom,
                        subject:mailAccountCreatedSubject,
                        html:mailAccountCreatedHTML
                    }).then(emailResult=>{
                        console.log(emailResult);
                    }).catch(err=>{
                        console.log(err);
                        throw new Error(err);
                    });
                    
                }).catch(err=>{
                    console.log(err);
                    throw new Error(err);
                });
                
            });
            
        });
}

/**
 * send password reset link to the received email
*/
exports.sendPasswordResetLink=(req,res,next)=>{
    let email=req.body.email;

    crypto.randomBytes(32,(err,buffer)=>{
        if(err){
            console.log(err);
            throw new Error(err);
        }
        const token = buffer.toString('hex');
        User.findOne({where:{
            email: email
        }}).then(user=>{
            if(!user){
                console.log('no user with this email id found');
            }
            user.resetToken = token;
            user.resetTokenExpiration = Date.now()+3600000;
            return user.save();

        }).then(result=>{
            console.log('mail from : '+ mailFrom);

            transporter.sendMail({
                to:email,
                from:mailFrom,
                subject:mailAccountResetTokenSubject,
                html:`
                <p>Your request for to reset the password is received, please click on the password reset <a href="${appUrl}/passwordResetProcess/${token}">link</a> to reset your password.</p>`
            }).then(emailResult=>{
                console.log(emailResult);
                res.json(emailResult);
            }).catch(err=>{
                console.log(err);
                throw new Error(err);
            });
        }).catch(err=>{
            console.log(err);
            throw new Error(err);
        })
    })
}

/**
 * verify the token received and update with new password.
*/
exports.resetPassword=(req, res, next)=>{
    const token = req.body.token;
    const email = req.body.email;
    const newPassword = req.body.password;
    let resetUser;

    User.findOne({where:{
        resetToken: token,
        resetTokenExpiration: {
            [Op.gt]: Date.now()
          }
    }}).then(user=>{
        if(!user){
            console.log('User not found, either token not found or token is expired');
        }
        resetUser = user;
        return bcrypt.hash(newPassword,12);

    }).then(hashedPassword=>{
        resetUser.password = hashedPassword;
        resetUser.resetToken = null;
        resetUser.resetTokenExpiration = null;
        return resetUser.save();
    }).then(result=>{
        res.json(result);
    }).catch(err=>{
        console.log(err);
        throw new Error(err);
    })
}