const Customer = require('../models/customer.model');
const Due = require('../models/due.model');
const Log = require('../models/log.model');
let logMessage = '';

/**
 * get all the existing dues
*/
exports.getDues = (req, res, next) => {
    const limit = parseInt(req.params.limit);
    const offset = parseInt(req.params.offset);

    Due.findAll({
        include: [{
            model: Customer,
        }],
        order: [
            ['createdAt', 'DESC']
        ],
        offset: offset,
        limit: limit
    }
    ).then(due => {
        return res.json(due);
    }).catch(err => {
        console.log(err);
        throw new Error(err);
    });
};

exports.addDue = (req, res, next) => {
    const amount = req.body.amount;
    const takenBy = req.body.takenBy;
    const promisedPaymentDate = req.body.promisedPaymentDate;
    const date = req.body.date;
    const note = req.body.note;
    const customerId = req.body.customerId;
    const interestRate = req.body.interestRate;
    let newDue = {
        amount: amount,
        takenBy: takenBy,
        promisedPaymentDate: promisedPaymentDate,
        date: date,
        note: note,
        interestRate: interestRate,
        customerId: customerId
    };

    Due.create(newDue).then(due => {
        logMessage = '<h5>Due created </h5>' + JSON.stringify(newDue);
        Log.create({
            eventName: logMessage,
            customerId: customerId
        }).then(result => {
            console.log('log created');
        }).catch(err => {
            console.log('error while creating log', err);
        });
        return res.json(due);
    }).catch(err => {
        console.log(err);
        throw new Error(err);
    })

}

exports.removeDue = (req, res, next) => {
    const dueId = req.params.dueId;
    Due.findOne({
        where: {
            id: dueId
        }
    }).then(result => {
        if (result) {
            logMessage = '<h5>Due deleted </h5>' + JSON.stringify(result);
            Log.create({
                eventName: logMessage,
                customerId: result.customerId
            }).then(result => {
                console.log('log created');
            }).catch(err => {
                console.log('error while creating log', err);
            });
            return result.destroy();
        }
    }).then(result => {
        console.log('due deleted successfully');
        res.json(result);
    })
        .catch(err => {
            console.log(err);
            throw new Error(err);
        });
}

exports.updateDue = (req, res, next) => {
    const amount = req.body.amount;
    const takenBy = req.body.takenBy;
    const promisedPaymentDate = req.body.promisedPaymentDate;
    const date = req.body.date;
    const note = req.body.note;
    const customerId = req.body.customerId;
    const interestRate = req.body.interestRate;
    const dueId = req.body.id;
    let updatedDue = {
        amount: amount,
        takenBy: takenBy,
        promisedPaymentDate: promisedPaymentDate,
        date: date,
        note: note,
        interestRate: interestRate,
        customerId: customerId
    };
    Due.update(updatedDue, {
        where: {
            id: dueId
        }
    }).then(due => {
        logMessage = '<h5>Due updated to </h5>' + JSON.stringify(updatedDue);
        Log.create({
            eventName: logMessage,
            customerId: customerId
        }).then(result => {
            console.log('log created');
        }).catch(err => {
            console.log('error while creating log', err);
        });
        return res.json(due);
    }).catch(err => {
        console.log(err);
        throw new Error(err);
    })
}

exports.duesOfCustomer = (req, res, next) => {
    const customerId = req.params.customerId;
    Due.findAll({
        where: {
            customerId: customerId
        }
    }).then(dues => {
        res.json(dues);
    }).catch(err => {
        console.log(err);
        throw new Error(err);
    })
}

exports.getDuesCount = (req, res, next) => {
    Due.count().then(count => {
        res.json(count);
    }).catch(err => {
        console.log(err);
        throw new Error(err);
    });
}