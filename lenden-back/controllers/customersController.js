const Customer = require('../models/customer.model');
const Due = require('../models/due.model');
const Sequelize = require('sequelize');
const Collection = require('../models/collection.model');
const Log = require('../models/log.model');

let logMessage='';
/**
 * get all the existing customers
*/
exports.getCustomers = (req, res, next) => {
    Customer.findAll({
        include: [{
            model: Due,
            order: [['date', 'ASC']],
            attributes: {
                include: [[
                    Sequelize.literal(`(
                    SELECT SUM(dues.amount)
                    FROM dues AS dues
                    WHERE
                    customer.id = dues.customerId
                )`),
                    'totalDues'
                ]]
            },
            // limit: 1
        }]
    }).then(customers => {
        return res.json(customers);
    }).catch(err => {
        console.log(err);
        throw new Error(err);
    });
};

exports.getCustomer = (req, res, next) => {
    const customerId = req.params.customerId;
    Customer.findOne({
        where: {
            id: customerId
        }
    }).then(customer => {
        res.json(customer);
    }).catch(err => {
        console.log(err);
        throw new Error(err);
    })
}


exports.addNewCustomer = (req, res, next) => {
    const name = req.body.name;
    const address = req.body.address;
    const contact = req.body.contact;
    const note = req.body.note;
    const referredBy = req.body.referredBy;
    const guardian = req.body.guardian;

    Customer.create({
        name: name,
        address: address,
        contact: contact,
        note: note,
        referredBy: referredBy,
        guardian: guardian
    }).then(result => {
        logMessage = '<h5>Created a new customer </h5>' + result.name ;
        Log.create({
            eventName: logMessage,
            customerId: result.id
        }).then(result=>{
            console.log('log created');
        }).then(err=>{
            console.log('error while creating log', err);
        });
        return res.json(result);
    }).catch(err => {
        console.log(err);
        throw new Error(err);
    });
}

exports.updateCustomer = (req, res, next) => {
    const customerId = req.body.id;
    const name = req.body.name;
    const address = req.body.address;
    const contact = req.body.contact;
    const note = req.body.note;
    const referredBy = req.body.referredBy;
    const guardian = req.body.guardian;
    const updatedCustomer = {
        name: name,
        address: address,
        contact: contact,
        note: note,
        referredBy: referredBy,
        guardian: guardian
    };

    Customer.update(updatedCustomer, {
        where: {
            id: customerId
        }
    }).then(result => {
        logMessage = '<h5>Customer Updated to </h5>' + JSON.stringify(updatedCustomer) ;
        Log.create({
            eventName: logMessage,
            customerId: customerId
        }).then(result=>{
            console.log('log created');
        }).catch(err=>{
            console.log('error while creating log', err);
        });
        return res.json(result);
    }).catch(err => {
        console.log(err);
        throw new Error(err);
    });
}

exports.deleteCustomer = (req, res, next) => {
    const customerId = req.params.customerId;
    let customResult={};
    // deleting a customer will delete all associated dues and collections

    // delete collections
    Log.destroy({
        where: {
            customerId: customerId
        }
    })


    // delete collections
    Collection.destroy({
        where: {
            customerId: customerId
        }
    })

    // delete dues
    Due.destroy({
        where: {
            customerId: customerId
        }
    });

    // delete customer
    Customer.destroy({
        where: {
            id: customerId
        }
    }).then(logDeletedResult => {
        customResult.collection = logDeletedResult;
    }).then(collectionDeletedResult => {
        customResult.collection = collectionDeletedResult;
    }).then(dueDeletedResult => {
        customResult.due = dueDeletedResult;
    }).then(customerDeletedResult => {
        logMessage = '<h5>Customer deleted </h5>(with its dues and collections) with id ' + customerId;
        Log.create({
            eventName: logMessage,
            customerId: null
        }).then(result=>{
            console.log('log created');
        }).catch(err=>{
            console.log('error while creating log', err);
        });
        customResult.customer = customerDeletedResult;
        return res.json(customResult);
    }).catch(err => {
        console.log(err);
        throw new Error(err);
    })
}

exports.findCustomers = (req, res, next) => {
    const searchTerm = '%' + req.params.searchTerm + '%';
    const { Op } = require("sequelize");

    Customer.findAll({
        include: [{
            model: Due,
            // limit: 1,
            order: [['date', 'ASC']],
            attributes: {
                include: [[
                    Sequelize.literal(`(
                        SELECT SUM(dues.amount)
                        FROM dues AS dues
                        WHERE
                        customer.id = dues.customerId
                    )`),
                    'totalDues'
                ]]
            }
        }],
        where: {
            [Op.or]: [
                {
                    name: {
                        [Op.like]: searchTerm
                    }
                },
                {
                    contact: {
                        [Op.like]: searchTerm
                    }
                },
                {
                    address: {
                        [Op.like]: searchTerm
                    }
                },
                {
                    guardian: {
                        [Op.like]: searchTerm
                    }
                }
            ]
        }
    }).then(result => {
        res.json(result);
    }).catch(err => {
        console.log(err);
        throw new Error(err);
    })
}

