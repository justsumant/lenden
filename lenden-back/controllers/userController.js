const User = require('../models/user');

/**
 * get all the existing user/customer
*/
exports.getUsers = (req, res, next) => {
    User.findAll().then(users => {
        return res.json(users);
    }).catch(err => {
        console.log(err);
    });
};

/**
* add a new user/customer
*/
exports.addUser = (req, res) => {
    const shopName = req.body.shopName;
    const startingDate = req.body.startingDate;
    const endsBy = req.body.endsBy;
    const name = req.body.name;
    User.create({
        name: name,
        shopName: shopName,
        startingDate: startingDate,
        endsBy: endsBy
    }).then(result => {
        res.json(result);
    }).catch(error => {
        console.log('error while saving new user...');
        throw new Error(error);
    });
}


/**
* delete a new user/customer
*/
exports.removeUser = (req, res) => {
    const userId = req.params.userId;
    User.destroy({
        where: {
            id: userId
        }
    }).then(result => {
        console.log('user deleted successfully:', result);
        res.json(result);
    }).catch(err => {
        console.log(err);
        throw new Error(err);
    });

}


/**
* update a new user/customer
*/
exports.editUser = (req, res) => {
    const shopName = req.body.shopName;
    const startingDate = req.body.startingDate;
    const endsBy = req.body.endsBy;
    const name = req.body.name;
    const userId = req.body.id;
    User.update({
        name: name,
        shopName: shopName,
        startingDate: startingDate,
        endsBy: endsBy
    }, {
        where: {
            id: userId
        }
    }).then(result => {
        console.log('user updated successfully..');
        res.json(result);
    }).catch(error => {
        console.log('error while updating user...');
        throw new Error(error);
    });
}
