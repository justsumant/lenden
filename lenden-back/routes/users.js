const path = require('path');
const express = require('express');
const userController = require('../controllers/userController');
const customersController = require('../controllers/customersController');
const dueController = require('../controllers/duesController');
const collectionController = require('../controllers/collectionsController');
const logsController = require('../controllers/logsController');
const isAuth = require('../middleware/isAuth');
const router = express.Router();

// old
router.get('/usersList', isAuth, userController.getUsers);
router.post('/addUser', isAuth, userController.addUser);
router.get('/removeUser:userId', isAuth, userController.removeUser);
router.post('/updateUser', isAuth, userController.editUser);

// customer routes
router.get('/getCustomers', isAuth, customersController.getCustomers);
router.post('/addNewCustomer', isAuth, customersController.addNewCustomer);
router.get('/deleteCustomer:customerId', isAuth, customersController.deleteCustomer);
router.post('/updateCustomer', isAuth, customersController.updateCustomer);
router.get('/getCustomer:customerId', isAuth, customersController.getCustomer);
router.get('/findCustomers:searchTerm', isAuth, customersController.findCustomers);


// due routes
router.get('/getDues/limit/:limit/offset/:offset', isAuth, dueController.getDues);
router.post('/addDue', isAuth, dueController.addDue);
router.get('/deleteDue:dueId', isAuth, dueController.removeDue);
router.post('/updateDue', isAuth, dueController.updateDue);
router.get('/duesOfCustomer:customerId', isAuth, dueController.duesOfCustomer);
router.get('/getDuesCount', isAuth, dueController.getDuesCount);

// collection routes
router.get('/getCollections/limit/:limit/offset/:offset', isAuth, collectionController.getCollections);
router.post('/addCollection', isAuth, collectionController.addCollection);
router.get('/deleteCollection:collectionId', isAuth, collectionController.removeCollection);
router.post('/updateCollection', isAuth, collectionController.updateCollection);
router.get('/collectionsOfCustomer:customerId', isAuth, collectionController.collectionsOfCustomer);
router.get('/getCollectionsCount', isAuth, collectionController.getCollectionsCount);

// logs routes
router.get('/getAllLogs/fromDate/:fromDate/toDate/:toDate/limitStart/:limitStart/limitSize/:limitSize', isAuth, logsController.getAllLogs);
router.get('/getCustomersLogs', isAuth, logsController.getCustomersLogs);
router.get('/getLogSize/fromDate/:fromDate/toDate/:toDate/', isAuth, logsController.getLogSize);



module.exports = router;