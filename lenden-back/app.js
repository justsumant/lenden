const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const db = require('./utils/database');
const userRoutes = require('./routes/users');
const adminRoutes = require('./routes/admin');
const bcrypt = require('bcryptjs');
const dotenv = require('dotenv');
dotenv.config();

const User = require('./models/user');
const Customer = require('./models/customer.model');
const Due = require('./models/due.model');
const Collection = require('./models/collection.model');
const Log = require('./models/log.model');


app.use(bodyParser.json());

app.use((req, res, next) => {
    console.log('welcome to backend... ', req.url, req.params);
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT, PATCH');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
});

app.use('', userRoutes);
app.use('/auth', adminRoutes);


//defining relationships
Customer.hasMany(Due);
Due.belongsTo(Customer);
Customer.hasMany(Collection);
Collection.belongsTo(Customer);
Customer.hasMany(Log);




db.sync(
    // { force: true }
).then(result => {
    // console.log(result);

    Customer.create({
        name: 'jagdish karn',
        address: 'matihani 09',
        contact: 987612340,
        note: 'this is customer note',
        referredBy: 'ganesh singh',
        guardian: 'jignesh karn'
    });

    Collection.create({
        amount: 20000,
        submittedBy: 'sumant gupta',
        date: new Date(),
        note: 'this is collection note...',
        customerId: 1
    });

    Due.create({
        amount: 20000,
        takenBy: 'aashish gupta',
        promisedPaymentDate: new Date(),
        date: new Date(),
        note: 'this is due note',
        customerId: 1
    });

    // adding additional logic, this will help while adding signup functionality which is not in our currennt project scope
    User.findOne({ where: { email: 'guptasumant517@gmail.com' } })
        .then(user => {
            if (user) {
                console.log('guptasumant517@gmail.com already exists, skipping creation of new user.');
                return;
            }
            bcrypt.hash('justsumant', 12).then(hashedPassword => {
                User.create({
                    name: 'sumant gupta',
                    email: 'guptasumant517@gmail.com',
                    password: hashedPassword
                });
                return;
            });

        });



}).then( customer=> {
    console.log('1 Customer created...');
}).then( collectionResult=> {
    console.log('1 collection created...');
}).then( dueResult=> {
    console.log('1 due created...');
}).then(userResult => {
    console.log('1 user created...');
    // app.listen(3000);
}).catch(err => {

    console.log(err);
}).finally(() => {
    app.listen(3000);
});

