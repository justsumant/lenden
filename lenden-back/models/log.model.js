const Sequelize = require('sequelize');
const sequelize = require('../utils/database');

const Log = sequelize.define('log', {
    id:{
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    eventName: Sequelize.STRING,
    customerId: Sequelize.INTEGER
});
module.exports = Log;