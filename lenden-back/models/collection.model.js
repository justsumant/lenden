const Sequelize = require('sequelize');
const sequelize = require('../utils/database');

const Collection = sequelize.define('collection', {
    id:{
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    amount: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    submittedBy: Sequelize.STRING,
    dueId: Sequelize.INTEGER,
    date: Sequelize.DATEONLY,
    note: Sequelize.STRING,
    customerId: Sequelize.INTEGER,
    interestRate: {
        type: Sequelize.DOUBLE,
        defaultValue: 0
    }


});

module.exports = Collection;