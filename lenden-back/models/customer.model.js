const Sequelize = require('sequelize');
const sequelize = require('../utils/database');

const Customer = sequelize.define('customer', {
    id:{
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    guardian:Sequelize.STRING,
    address: Sequelize.STRING,
    contact: Sequelize.BIGINT,
    note: Sequelize.STRING,
    referredBy: Sequelize.STRING


});

module.exports = Customer;