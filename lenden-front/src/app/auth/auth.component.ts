import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, NgForm, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthService } from "./auth.service";

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html'
})

export class AuthComponent implements OnInit{
userName: string='';
password: string='';
isProcessing:boolean=false;
authForm: any;
constructor(private authService:AuthService, private router: Router){
    // if(localStorage.getItem('token')){
    //     router.navigate(['/home/dashboard']);
    // }

}

ngOnInit(){
  this.authForm=new FormGroup({
    'username': new FormControl(null, [Validators.required, Validators.email]),
    'password': new FormControl(null, Validators.required)
  })
}


login(){
this.isProcessing=true;
this.authService.login(this.authForm.value.username, this.authForm.value.password).subscribe((data:any)=>{
    if(data.token){
      localStorage.setItem('token',data.token);
      localStorage.setItem('userId',data.loadedUserId);
      this.router.navigate(['/home/dashboard']);
    }
    this.isProcessing=false;
    this.authForm.reset();
    this.router.navigate(['/home/dashboard']);
  
  }, error=>{
    console.log(error);
    
  });



}
    }