import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  signupForm: any;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.signupForm = new FormGroup({
      'name': new FormControl(null, Validators.required),
      'username': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, [Validators.required, Validators.minLength(4)]),
      'confirmPassword': new FormControl(null, [Validators.required, Validators.minLength(4)])
    });
  }

  signup(){
    let name=this.signupForm.value.name;
    let email=this.signupForm.value.username;
    let password = this.signupForm.value.password;
    let confirmPassword = this.signupForm.value.confirmPassword;
    if(password === confirmPassword){
      let newUser:User={
        name: name,
        email: email,
        password: password
      }
      this.authService.signup(newUser).subscribe(result=>{
        console.log(result);
        this.router.navigate(['/auth']);
        
      },err=>{
        console.log(err);
        
      });
    }

  
  // form.reset();
  }

}
