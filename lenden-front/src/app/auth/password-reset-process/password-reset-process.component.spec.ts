import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PasswordResetProcessComponent } from './password-reset-process.component';

describe('PasswordResetProcessComponent', () => {
  let component: PasswordResetProcessComponent;
  let fixture: ComponentFixture<PasswordResetProcessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PasswordResetProcessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PasswordResetProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
