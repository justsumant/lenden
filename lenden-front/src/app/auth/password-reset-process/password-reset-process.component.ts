import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-password-reset-process',
  templateUrl: './password-reset-process.component.html',
  styleUrls: ['./password-reset-process.component.scss']
})
export class PasswordResetProcessComponent implements OnInit {
  passwordResetToken = '';
  isProcessing:boolean=false;
  resetPasswordForm: any;

  constructor(private activatedRoute: ActivatedRoute, private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    // this.activatedRoute.queryParams.subscribe((params: Params) => {
    //   console.log(params['token']);
    //   this.passwordResetToken = params['token'];
    // });
    this.resetPasswordForm = new FormGroup({
      "password": new FormControl(null, [Validators.minLength(4), Validators.required]),
      "confirmPassword": new FormControl(null, [Validators.required, Validators.minLength(4)])
    });
    this.passwordResetToken = this.activatedRoute.snapshot.params['token'];    
  }

  resetPassword() {
    this.isProcessing=true;
    let password = this.resetPasswordForm.value.password;
    let confirmPassword = this.resetPasswordForm.value.confirmPassword;
    if (password === confirmPassword) {
      let pwdObject = {
        password: password,
        token: this.passwordResetToken
      }
      this.authService.updatePassword(pwdObject).subscribe(result => {
        console.log('password reset successfull...');
        this.isProcessing=false;
        this.router.navigate(['/auth']);
      }, err => {
        this.isProcessing=true;
        console.log(err);

      });
    }



  }

}
