import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-password-reset-request',
  templateUrl: './password-reset-request.component.html',
  styleUrls: ['./password-reset-request.component.scss']
})
export class PasswordResetRequestComponent implements OnInit {
  emailSentFlag:boolean = false;
  email: string='';
  isProcessing:boolean=false;
  resetPasswordForm:any;
  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit(): void {
    this.resetPasswordForm = new FormGroup({
      "email": new FormControl(null, [Validators.required, Validators.email])
    })
  }

  sendPasswordResetMail(){
    this.isProcessing=true;
    this.authService.sendPasswordResetLink(this.resetPasswordForm.value.email).subscribe(result=>{
      this.isProcessing=false;      
      this.emailSentFlag=true;
    }, err=>{
      console.log(err);
      
    })
    
  }

  gotoLogin(){
this.router.navigate(['/auth']);
  }

}
