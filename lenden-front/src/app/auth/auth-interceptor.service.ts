import { HttpErrorResponse, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { throwError } from "rxjs";
import {catchError, tap} from "rxjs/operators";

export class authInterceptor implements HttpInterceptor{
    // baseURL: string = 'http://localhost:3000';
    baseURL: string = 'https://api.verisoft.com.np';
    intercept(req: HttpRequest<any>, next: HttpHandler){
        let modifiedRequest = req.clone({
            url: this.baseURL+req.url,
            headers: req.headers.append('Authorization','Bearer ' + localStorage.getItem('token'))
        })


        // return next.handle(modifiedRequest).pipe(
        //     tap((event:any)=>{
        //         if(event.type = HttpEventType.Response){
        //             console.log(event.body);
                    
        //         }
        //     })
        // )


        // return next.handle(modifiedRequest).pipe(
        //         catchError((error: HttpErrorResponse)=>{
        //             let errorMsg = '';
        //             if (error.error instanceof ErrorEvent) {
        //                 errorMsg = `Error: ${error.error.message}`;
        //             }
        //             else {
        //                 errorMsg = `Error Code: ${error.status},  Message: ${error.message}`;
        //             }
        //             console.log(errorMsg);
        //             return throwError(errorMsg);
        //         })
        // )
        
        return next.handle(modifiedRequest);
    }
}