import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
// baseURL: string = 'http://localhost:3000';
baseURL: string = 'https://api.verisoft.com.np'


  constructor(private router: Router, private http: HttpClient) { 
    
  }

  login(userName: String, password: String){
    let user = {userName: userName, password: password};
    return this.http.post('/auth/login', user);


    // this.isLoggedIn=true;
    // localStorage.setItem('isLoggedIn',this.isLoggedIn.toString());
  }

  signup(user:any){
    return this.http.post('/auth/signup', user);
  }

  sendPasswordResetLink(email: string){
    let emailObject={
      email: email
    };
    return this.http.post('/auth/send-password-reset-link',emailObject);
  }

  updatePassword(pwdObj:any){
    return this.http.post('/auth/reset-password', pwdObj);
  }
}
