import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Customer } from '../models/customer.model';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  today = new Date().getFullYear() + '-' + new Date().getMonth() + '-' + new Date().getDate();
  selectedCustomerForCustomerDetails: any={};


  constructor(private http: HttpClient) { }


  getCustomers() {
    return this.http.get('/getCustomers');
  }

  addNewCustomer(customer: Customer){
    return this.http.post('/addNewCustomer',customer);
  }

  editCustomer(customer: Customer){
    return this.http.post('/updateCustomer',customer);
  }

  deleteCustomer(customerId:number){
    return this.http.get('/deleteCustomer'+customerId);
  }

  getCustomer(customerId: number){
    return this.http.get('/getCustomer'+customerId);
  }

  findCustomers(searchTerm:string){
    console.log(searchTerm);
    
    return this.http.get('/findCustomers'+searchTerm);
  }
}
