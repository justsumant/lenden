import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  baseURL: string = 'http://localhost:3000';
  // baseURL: string = 'https://api.verisoft.com.np'

  today = new Date().getFullYear() + '-' + new Date().getMonth() + '-' + new Date().getDate();


  constructor(private http: HttpClient) { }

  // getHeader() {
  //   return ({
  //     headers: {
  //       Authorization: 'Bearer ' + localStorage.getItem('token')
  //     }
  //   });
  // }

  addUser(user: User) {
    return this.http.post('/addUser', user);
  }

  deleteUser(id: number) {
    return this.http.get('/removeUser' + id);
  }

  getUsers() {
    return this.http.get('/usersList');
  }

  updateUser(user: User) {
    console.log(user);

    return this.http.post('/updateUser', user)
  }
}
