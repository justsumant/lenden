import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Collection } from '../models/collection.model';

@Injectable({
  providedIn: 'root'
})
export class CollectionsService {

  today = new Date().getFullYear() + '-' + new Date().getMonth() + '-' + new Date().getDate();


  constructor(private http: HttpClient) { }

  getCollections(limit:number, offset:number) {
    return this.http.get('/getCollections/limit/'+limit+'/offset/'+offset+'/');
  }

  addCollection(collection: Collection){
    return this.http.post('/addCollection', collection);
  }

  updateCollection(collection: Collection){
    console.log(collection);
    return this.http.post('/updateCollection', collection);
  }

  deleteCollection(collectionId: number){
    return this.http.get('/deleteCollection' + collectionId);
  }

  getCollectionsOfCustomer(customerId: number){
    return this.http.get('/collectionsOfCustomer' + customerId);
  }

  getCollectionsCount(){
    return this.http.get('/getCollectionsCount');
  }
}


