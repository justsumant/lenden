import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LogsService {

  today = new Date().getFullYear() + '-' + new Date().getMonth() + '-' + new Date().getDate();


  constructor(private http: HttpClient) { }

  getAllLogs(fromDate: string, toDate: string, limitStart: number, limitSize:number){
    return this.http.get('/getAllLogs/fromDate/'+fromDate+'/toDate/'+toDate+'/limitStart/'+limitStart+'/limitSize/'+limitSize+'/');
  }

  getLogSize(fromDate: string, toDate: string){
    return this.http.get('/getLogSize/fromDate/'+fromDate+'/toDate/'+toDate+'/');
  }

}
