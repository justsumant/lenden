import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Due } from '../models/due.model';

@Injectable({
  providedIn: 'root'
})
export class DuesService {

  today = new Date().getFullYear() + '-' + new Date().getMonth() + '-' + new Date().getDate();


  constructor(private http: HttpClient) { }


  getDues(limit:number, offset:number) {
    return this.http.get('/getDues/limit/'+limit+'/offset/'+offset+'/');
  }

  addDue(due: Due){
    return this.http.post('/addDue', due);
  }

  deleteDue(dueId: number){
    return this.http.get('/deleteDue'+dueId);
  }

  updateDue(due: Due){
    console.log(due);
    
    return this.http.post('/updateDue',due);
  }

  getDuesOfCustomer(customerId: number){
    return this.http.get('/duesOfCustomer'+customerId);
  }

  getDuesCount(){
    return this.http.get('/getDuesCount');
  }
}

