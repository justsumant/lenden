import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { HomeComponent } from './components/home/home.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationDialogService } from './dialog/confirmation-dialog/confirmation-dialog.service';
import { ConfirmationDialogComponent } from './dialog/confirmation-dialog/confirmation-dialog.component';
import { authInterceptor } from './auth/auth-interceptor.service';
import { CustomersComponent } from './components/customers/customers.component';
import { DuesComponent } from './components/dues/dues.component';
import { CollectionsComponent } from './components/collections/collections.component';
import { CustomerDetailsComponent } from './components/customer-details/customer-details.component';
import { AddDueModalComponent } from './modals/add-due-modal/add-due-modal.component';
import { AddCollectionModalComponent } from './modals/add-collection-modal/add-collection-modal.component';
import { PasswordResetRequestComponent } from './auth/password-reset-request/password-reset-request.component';
import { PasswordResetProcessComponent } from './auth/password-reset-process/password-reset-process.component';
import { SignupComponent } from './auth/signup/signup.component';
import { AddCustomerModalComponent } from './modals/add-customer-modal/add-customer-modal.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { ToastService } from './dialog/toast/toast-service';
import { ToastsContainer } from './dialog/toast/toasts-container.component';
import { LogsComponent } from './components/logs/logs.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    HomeComponent,
    DashboardComponent,
    ConfirmationDialogComponent,
    CustomersComponent,
    DuesComponent,
    CollectionsComponent,
    CustomerDetailsComponent,
    AddDueModalComponent,
    AddCollectionModalComponent,
    PasswordResetRequestComponent,
    PasswordResetProcessComponent,
    SignupComponent,
    AddCustomerModalComponent,
    ToastsContainer,
    LogsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    NgSelectModule
  ],
  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    ConfirmationDialogService, ToastService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: authInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
