import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { AuthGuard } from './auth/auth.guard';
import { CollectionsComponent } from './components/collections/collections.component';
import { CustomerDetailsComponent } from './components/customer-details/customer-details.component';
import { CustomersComponent } from './components/customers/customers.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DuesComponent } from './components/dues/dues.component';
import { HomeComponent } from './components/home/home.component';
import { PasswordResetRequestComponent } from './auth/password-reset-request/password-reset-request.component';
import { PasswordResetProcessComponent } from './auth/password-reset-process/password-reset-process.component';
import { SignupComponent } from './auth/signup/signup.component';
import { LogsComponent } from './components/logs/logs.component';

const routes: Routes = [
  {path:'', redirectTo:'/auth', pathMatch: 'full'},
  {path:'auth', component: AuthComponent },
  {path:'auth/passwordReset', component: PasswordResetRequestComponent },
  {path:'auth/signup', component: SignupComponent },
  {path:'auth/passwordResetProcess/:token', component: PasswordResetProcessComponent },
  {path:'home', canActivate:[AuthGuard], component: HomeComponent,
  children:[
    { path:'customers', component: CustomersComponent},
    { path:'customer-detail', component: CustomerDetailsComponent},
    { path:'dashboard', component: DashboardComponent },
    { path:'dues', component: DuesComponent },
    { path:'collections', component: CollectionsComponent },
    { path:'logs', component: LogsComponent }
  ]
  },
  {path:'*', redirectTo:'/auth'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
