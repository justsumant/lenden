import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Customer } from 'src/app/models/customer.model';
import { CustomerService } from 'src/app/services/customer.service';
import { LogsService } from 'src/app/services/logs.service';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.scss']
})
export class LogsComponent implements OnInit {
  logs: any = [];
  selectedCustomer: any = {};
  customers: any = [];
  fromDate: string = '';
  toDate: string = '';

  page = 1;
  limitStart = 0;
  limitSize = 10;
  pageSize = 0;

  logsLoaded:boolean =false;


  constructor(
    private logsService: LogsService,
    private customerService: CustomerService,
    private router: Router) { }

  ngOnInit(): void {
    if (this.customerService.selectedCustomerForCustomerDetails) {
      this.selectedCustomer = this.customerService.selectedCustomerForCustomerDetails;
    }
    this.getLogSize();
  }

  pageChange() {
    this.limitStart = this.limitSize * (this.page-1);
    // console.log('pageSize: ', this.pageSize, ' page no: ', this.page, ' limitSize: ', this.limitSize, ' limitStart: ', this.limitStart);
    this.getLogs();

  }

  getLogSize() {
    this.logsService.getLogSize(this.fromDate || ' ', this.toDate || ' ').subscribe((size: any) => {
      this.pageSize = size;
      this.getLogs();
      // console.log('pageSize: ', this.pageSize, ' page no: ', this.page, ' limitSize: ', this.limitSize, ' limitStart: ', this.limitStart);
    }, err => {
      console.log(err);

    })
  }

  /**
   * get all the customers (no filter)
  */
  getCustomers() {
    this.customerService.getCustomers().subscribe((customers: any) => {
      this.customers = customers.map((i: any) => {
        i.bindLabel = i.name + ', ' + i.address + ', ' + i.contact;
        return i;
      });
    }, err => {
      console.log(err);
    });
  }


  /**
   * retrieve all the logs for listing
  */
  getLogs() {
    this.logsLoaded=false;
    this.logsService.getAllLogs(this.fromDate || ' ', this.toDate || ' ', this.limitStart, this.limitSize).subscribe(logs => {
      this.logs = logs;
      this.logsLoaded=true;
    }, err => {
      console.log(err);
    });
  }

  /**
   * select the due and take to customer detail associated with it
  */
  viewCustomer(customerId: number) {
    this.customerService.getCustomer(customerId).subscribe(customer => {
      this.customerService.selectedCustomerForCustomerDetails = customer;
      this.router.navigate(['/home/customer-detail']);
    }, err => {
      console.log(err);
    });


  }


  
}
