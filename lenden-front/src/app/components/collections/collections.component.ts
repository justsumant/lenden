import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CollectionsService } from 'src/app/services/collections.service';
import { CustomerService } from 'src/app/services/customer.service';

@Component({
  selector: 'app-collections',
  templateUrl: './collections.component.html',
  styleUrls: ['./collections.component.scss']
})
export class CollectionsComponent implements OnInit {
  collections: any = [];
  page = 1;
  limitStart = 0;
  limitSize = 10;
  pageSize = 0;
  collectionSize = 0;
  collectionsLoaded:boolean=false;

  constructor(
    private collectionService: CollectionsService, 
    private customerService: CustomerService, 
    private router: Router) { }

  ngOnInit(): void {
    this.getCollectionsCount();
  }

  pageChange() {
    this.limitStart = this.limitSize * (this.page-1);
    console.log('pageSize: ', this.pageSize, ' page no: ', this.page, ' limitSize: ', this.limitSize, ' limitStart: ', this.limitStart);
    this.getAllCollections();

  }

  getCollectionsCount(){
    this.collectionService.getCollectionsCount().subscribe((size:any)=>{
      this.collectionSize = size;
      this.getAllCollections();
    }, err=>{
      console.log(err);
      
    });
  }

  /**
   * gets all the collections available for listing
  */
  getAllCollections() {
    this.collectionsLoaded=false;
    this.collectionService.getCollections(this.limitSize, this.limitStart).subscribe(collections => {
      this.collections = collections;
      this.collectionsLoaded=true;
    }, err => {
      console.log(err);
    })
  }

  /**
   * direct user to customer detail related to selected collection
  */
  gotoCustomerDetail(customer: any) {
    this.customerService.selectedCustomerForCustomerDetails = customer;
    this.router.navigate(['/home/customer-detail']);
  }
}
