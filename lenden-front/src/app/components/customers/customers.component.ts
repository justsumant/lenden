import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ConfirmationDialogService } from 'src/app/dialog/confirmation-dialog/confirmation-dialog.service';
import { ToastService } from 'src/app/dialog/toast/toast-service';
import { Customer } from 'src/app/models/customer.model';
import { CustomerService } from 'src/app/services/customer.service';
import * as moment from 'moment';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {
  searchCustomerString: string = '';
  customers: any = [];
  isCustomerLoaded:boolean = false;
  updateCustomerForm:any;
  @ViewChild('editCustomerCanvasClose') customerCanvasButton: any;

  constructor(
    private customerService: CustomerService,
    private router: Router,
    private confirmationDialogueService: ConfirmationDialogService,
    private toastService: ToastService) { }

  ngOnInit(): void {
    this.getCustomers();

    this.updateCustomerForm= new FormGroup({
      "id":new FormControl(null, [Validators.required]),
      "name": new FormControl(null, [Validators.required, Validators.minLength(4)]),
      "guardian": new FormControl(null, [ Validators.minLength(4)]),
      "address": new FormControl(null, [Validators.required, Validators.minLength(4)]),
      "contact":new FormControl(null, [Validators.pattern(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/)]),
      "referredBy":new FormControl(null, Validators.minLength(4)),
      "note":new FormControl(null)
    });
  }

  /**
   * retrieves event from add customer model, then refreshes the list
  */
  customerAddedEvent(event: any) {
    if (event) {
      this.getCustomers();
    }

  }

  /**
   * retrieve all the customers for the list
  */
  getCustomers() {
    this.isCustomerLoaded=false;
    this.customers=[];
    if (!this.searchCustomerString.trim()) {
      this.customerService.getCustomers().subscribe((customers:any) => {
        this.customers = customers;
        // this.customers = customers.map((customer: any) => {           
        //   if(customer.dues.length>0){
        //     customer.dues[0].date = moment(customer.dues[0].date).format('DD MMM YYYY'); 
        //     customer.dues[0].promisedPaymentDate = moment(customer.dues[0].promisedPaymentDate).format('DD MMM YYYY'); 
        //   }
        //   return customer; 
        // });
        this.isCustomerLoaded=true;
      }, error => {
        console.log(error);
        if (error.status == 401)
          this.router.navigate(['/auth']);
      })
    } else {
      this.customerService.findCustomers(this.searchCustomerString).subscribe((customers:any) => {
        this.customers = customers;
        // this.customers = customers.map((customer: any) => {           
        //   if(customer.dues.length>0){
        //     customer.dues[0].date = moment(customer.dues[0].date).format('DD MMM YYYY'); 
        //     customer.dues[0].promisedPaymentDate = moment(customer.dues[0].promisedPaymentDate).format('DD MMM YYYY'); 
        //   }
        //   return customer; 
        // });
        this.isCustomerLoaded=true;
      }, error => {
        console.log(error);
        if (error.status == 401)
          this.router.navigate(['/auth']);

      })
    }

  }

  /**
   * set selected customer as updatable to open in canvas
  */
  setUpdatableCustomer(customer: Customer) {
    this.updateCustomerForm.setValue({
      id: customer.id,
      name: customer.name,
      address: customer.address,
      guardian: customer.guardian,
      note: customer.note,
      contact: customer.contact,
      referredBy: customer.referredBy
   });
  }

  /**
   * updates selected customer
  */
  updateCustomer() {
    let name = this.updateCustomerForm.value.name;
    let address = this.updateCustomerForm.value.address;
    let contact = this.updateCustomerForm.value.contact;
    let note = this.updateCustomerForm.value.note;
    let referredBy = this.updateCustomerForm.value.referredBy;
    let guardian = this.updateCustomerForm.value.guardian;
    let customerId = this.updateCustomerForm.value.id;

    let newCustomer: Customer = {
      name: name,
      address: address,
      contact: contact,
      note: note,
      referredBy: referredBy,
      guardian: guardian,
      id: customerId
    }
    this.customerService.editCustomer(newCustomer).subscribe(result => {
      this.getCustomers();
      // close canvas programmatically on successfull editing of customer
      this.customerCanvasButton.nativeElement.click();
      this.toastService.show("Customer Updated", { classname: 'lenden-primary text-light', delay: 5000 });

    }, err => {
      console.log(err, 'something wrong happened at server.....');

    });

  }

  /**
   * select the customer and divert to customer details page
  */
  viewCustomer(customer: Customer) {
    this.customerService.selectedCustomerForCustomerDetails = customer;
    this.router.navigate(['/home/customer-detail']);
  }


  /**
   * deletes the selected customer
  */
  deleteCustomer(customerId: number) {
    this.confirmationDialogueService.confirm('Confirm Delete', 'This is going to permanently delete the customer with its logs, dues and collections which is irreversible.').then(result => {
      if (result) {
        this.customerService.deleteCustomer(customerId).subscribe(result => {
          this.getCustomers();
          this.toastService.show("Customer Deleted", { classname: 'lenden-primary text-light', delay: 5000 });
        }, err => {
          console.log(err);
        });
      }
    }).catch(err => {
      console.log(err, 'confimation dialogue closed by close button.');
    });
  }
}
