import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { CollectionsService } from 'src/app/services/collections.service';
import { CustomerService } from 'src/app/services/customer.service';
import { DuesService } from 'src/app/services/dues.service';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Collection } from 'src/app/models/collection.model';
import { Due } from 'src/app/models/due.model';
import { ConfirmationDialogService } from 'src/app/dialog/confirmation-dialog/confirmation-dialog.service';
import { ToastService } from 'src/app/dialog/toast/toast-service';
import * as moment from 'moment';
// moment.locale('ne');

@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.scss']
})
export class CustomerDetailsComponent implements OnInit {
  collections: any = [];
  dues: any = [];
  duesTotal: any;
  collectionsTotal: any;
  customers: any = [];
  totalDues: number = 0;
  totalCollections: number = 0;
  editableCollection: any = {};
  editableDue: any = {};
  selectedCustomer: any = {};
  totalCollectionInterest:number=0;
  totalDueInterest:number=0;
  today = moment();
  duesLoaded:boolean=false;
  collectionsLoaded: boolean=false;
  updateCollectionForm:any;
  updateDueForm:any;


  @ViewChild('editCollectionCanvasClose') collectionCanvasButton: any;
  @ViewChild('editDueCanvasClose') dueCanvasButton: any;

  constructor(
    private dueService: DuesService, 
    private collectionService: CollectionsService, 
    private customerService: CustomerService,
    private confirmationDialogueService: ConfirmationDialogService,
    private toastService: ToastService) { }

  ngOnInit(): void {
    // if previously selected customer is there, keep the same for next time customer detail page visit
    if(this.customerService.selectedCustomerForCustomerDetails){
      this.selectedCustomer = this.customerService.selectedCustomerForCustomerDetails;
    }
    this.searchCustomer();
    this.getCustomers();    

    this.updateCollectionForm=new FormGroup({
      id: new FormControl(null, Validators.required),
      submittedBy: new FormControl(null, Validators.required),
      startingDate: new FormControl(null, Validators.required),
      associatedTo: new FormControl(null, Validators.required),
      customerId:new FormControl(null, Validators.required),
      remarks: new FormControl(null, Validators.required),
      amount: new FormControl(null, Validators.required),
      interestRate: new FormControl(null, Validators.required)
    })

    this.updateDueForm=new FormGroup({
      id: new FormControl(null, Validators.required),
      amount: new FormControl(null, Validators.required),
      startingDate: new FormControl(null, Validators.required),
      takenBy: new FormControl(null, Validators.required),
      completionDate: new FormControl(null, Validators.required),
      remarks: new FormControl(null, Validators.required),
      customerId: new FormControl(null, Validators.required),
      rate: new FormControl(null, Validators.required)
    })
  }

  /**
   * get all the customers (no filter)
  */
  getCustomers() {
    this.customerService.getCustomers().subscribe((customers: any) => {
      this.customers = customers.map((i: any) => { 
        i.bindLabel = i.name + ', ' + i.address + ', ' + i.contact; 
        return i; 
      });
    }, err => {
      console.log(err);
    });
  }

  /**
   * search and bring customer based on search criteria in the input field
  */
  searchCustomer() {
    if (this.selectedCustomer.id) {
      this.customerService.getCustomer(this.selectedCustomer.id).subscribe((customer: any) => {
        this.selectedCustomer = customer;
        this.getDues(customer.id);
        this.getCollections(customer.id);
      }, err => {
        console.log(err);
      })
    }

  }

  /**
   * get dues of the selected customer
  */
  getDues(customerId: number) {
    this.duesLoaded=false;
    this.dueService.getDuesOfCustomer(customerId).subscribe((dues: any) => {
      this.dues = dues;
      this.totalDues = 0;
      this.totalDueInterest=0;
      // does total dues calculation
      if (dues && dues.length) {
        dues.forEach((due: any) => {
          this.totalDues += due.amount;
          let startDate = moment(due.date).format('DD MMM YYYY');
          let months = this.today.diff(startDate, 'months');
          let totalAmount=due.amount;
          if(due.interestRate>0){
            while(months>12){
              totalAmount += totalAmount*due.interestRate/100;
              months-=12;
            }
            totalAmount += totalAmount*due.interestRate/100*months/12;
          }
          due.totalAmount = totalAmount.toFixed(0); 
          this.totalDueInterest += totalAmount-due.amount;        
          
        });
        // this.dues = dues.map((due: any) => { 
        //   due.date = moment(due.date).format('DD MMM YYYY'); 
        //   this.totalDues += due.amount;
        //   return due; 
        // });
      }
      this.duesLoaded=true;
    }, err => {
      console.log(err);

    });
  }

  /**
   * get all collections of the selected customer
  */
  getCollections(customerId: number) {
    this.collectionsLoaded = false;
    this.collectionService.getCollectionsOfCustomer(customerId).subscribe((collections: any) => {
      this.collections = collections;
      this.totalCollections = 0;
      this.totalCollectionInterest=0;
      // does total collections calculation
      if (collections && collections.length) {
        collections.forEach((collection: any) => {
          this.totalCollections += collection.amount;
          let startDate = moment(collection.date).format('DD MMM YYYY');
          let months = this.today.diff(startDate, 'months');
          let totalAmount=collection.amount;
          if(collection.interestRate>0){
            while(months>12){
              totalAmount += totalAmount*collection.interestRate/100;
              months-=12;
            }
            totalAmount += totalAmount*collection.interestRate/100*months/12;
          }
          collection.totalAmount = totalAmount.toFixed(0); 
          this.totalCollectionInterest += totalAmount-collection.amount; 
        });
        // this.collections = collections.map((collection: any) => { 
        //   collection.date = moment(collection.date).format('DD MMM YYYY'); 
        //   this.totalCollections += collection.amount;
        //   return collection; 
        // });
      }
      this.collectionsLoaded = true;
    }, err => {
      console.log(err);

    })
  }

/**
 * this event is received when add-due model does a successfull addition of new Due
*/
  dueAddedEvent(event: any) {
    this.getDues(event);
  }

  /**
   * this event is received when add-collection model does a successfull addition of new Collection
  */
  collectionAddedEvent(event: any) {
    this.getCollections(event);
  }


  /**
   * updated the selected collection 
  */
  updateCollection() {
    let amount = this.updateCollectionForm.value.amount;
    let date = this.updateCollectionForm.value.startingDate;
    let dueId = this.updateCollectionForm.value.associatedTo;
    let note = this.updateCollectionForm.value.remarks;
    let submittedBy = this.updateCollectionForm.value.submittedBy;
    let collectionId = this.updateCollectionForm.value.id;
    let interestRate = this.updateCollectionForm.value.rate;

    let collection: Collection = {
      amount: amount,
      customerId: this.selectedCustomer.id,
      date: date,
      dueId: dueId,
      note: note,
      submittedBy: submittedBy,
      id: collectionId,
      interestRate: interestRate
    }

    this.collectionService.updateCollection(collection).subscribe(result => {
      this.toastService.show("Collection Updated", { classname: 'lenden-primary text-light', delay: 5000 });
      this.getCollections(this.selectedCustomer.id);
      // close canvas programatically after successfull updation
      this.collectionCanvasButton.nativeElement.click();
    }, err => {
      console.log(err);

    });

  }

  /**
   * updated selected due
  */
  udpateDue() {
    let amount = this.updateDueForm.value.amount;
    let remarks = this.updateDueForm.value.remarks;
    let date = this.updateDueForm.value.startingDate;
    let takenBy = this.updateDueForm.value.takenBy;
    let completionDate = this.updateDueForm.value.completionDate;
    let dueId = this.updateDueForm.value.id;
    let interestRate = this.updateDueForm.value.rate;

    let due: Due = {
      amount: amount,
      date: date,
      note: remarks,
      takenBy: takenBy,
      promisedPaymentDate: completionDate,
      customerId: this.selectedCustomer.id,
      interestRate: interestRate,
      id: dueId
    }
    this.dueService.updateDue(due).subscribe(result => {
      this.toastService.show("Due Updated", { classname: 'lenden-primary text-light', delay: 5000 });
      this.getDues(this.selectedCustomer.id);
      // close canvas programatically after successfull updation
      this.dueCanvasButton.nativeElement.click();
    }, err => {
      console.log(err);

    })

  }

  /**
   * sets the collection to edit, brings this collection in canvas
  */
  setEditableCollection(collection: Collection) {
    this.editableCollection = collection;
    console.log(collection);
    
    this.updateCollectionForm.setValue({
      id: collection.id,
      submittedBy: collection.submittedBy,
      startingDate: collection.date,
      associatedTo: collection.dueId,
      customerId: collection.customerId,
      remarks: collection.note,
      amount: collection.amount,
      interestRate: collection.interestRate
    })

  }

  /**
   * sets the due to edit, brings this due in canvas
  */
  setEditableDue(due: Due) {
    this.editableDue = due;
    this.updateDueForm.setValue({
      id: due.id,
      amount: due.amount,
      startingDate: due.date,
      takenBy: due.takenBy,
      completionDate: due.promisedPaymentDate,
      remarks: due.note,
      customerId: due.customerId,
      rate: due.interestRate
    })

  }

/**
 * deletes the selected
*/
  deleteDue(dueId:number){
    this.confirmationDialogueService.confirm('Confirm Delete', 'This is going to permanently delete the due which is irreversible.').then(result => {
      if (result) {
        this.dueService.deleteDue(dueId).subscribe(result=>{
          this.toastService.show("Due Deleted", { classname: 'lenden-primary text-light', delay: 5000 });
          this.getDues(this.selectedCustomer.id);
        },err=>{
          console.log(err);
          
        });
      }
    }).catch(err => {
      console.log(err, 'confimation dialogue closed by close button.');
    });
  }

  /**
   * deletes the selected collection
  */
deleteCollection(collectionId: number){
  this.confirmationDialogueService.confirm('Confirm Delete', 'This is going to permanently delete the collection which is irreversible.').then(result => {
    if (result) {
      this.collectionService.deleteCollection(collectionId).subscribe(result=>{
        this.toastService.show("Collection Deleted", { classname: 'lenden-primary text-light', delay: 5000 });
        this.getCollections(this.selectedCustomer.id);
      },err=>{
        console.log(err);
      })
    }
  }).catch(err => {
    console.log(err, 'confimation dialogue closed by close button.');
  });
}


}
