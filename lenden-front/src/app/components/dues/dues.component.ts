import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastService } from 'src/app/dialog/toast/toast-service';
import { CustomerService } from 'src/app/services/customer.service';
import { DuesService } from 'src/app/services/dues.service';

@Component({
  selector: 'app-dues',
  templateUrl: './dues.component.html',
  styleUrls: ['./dues.component.scss']
})
export class DuesComponent implements OnInit {
  dues: any = [];
  page = 1;
  limitStart = 0;
  limitSize = 10;
  pageSize = 0;
  dueSize = 0;
  duesLoaded:boolean=false;

  constructor(
    private duesService: DuesService, 
    private customerService: CustomerService, 
    private router: Router, 
    private toastService: ToastService) { }

  ngOnInit(): void {
    this.getDuesCount();
  }

  pageChange() {
    this.limitStart = this.limitSize * (this.page-1);
    console.log('pageSize: ', this.pageSize, ' page no: ', this.page, ' limitSize: ', this.limitSize, ' limitStart: ', this.limitStart);
    this.getAllDues();

  }

  getDuesCount(){
    this.duesService.getDuesCount().subscribe((size:any)=>{
      this.dueSize = size;
      this.getAllDues();
    }, err=>{
      console.log(err);
      
    });
  }

  /**
   * retrieve all the dues for listing
  */
  getAllDues() {
    this.duesLoaded=false;
    this.duesService.getDues(this.limitSize, this.limitStart).subscribe(dues => {
      this.dues = dues;
      this.duesLoaded=true;
    }, err => {
      console.log(err);
    })
  }

  /**
   * go to the customer of selected due
  */
  gotoCustomerDetail(customer: any) {
    this.customerService.selectedCustomerForCustomerDetails = customer;
    this.router.navigate(['/home/customer-detail']);
  }


}
