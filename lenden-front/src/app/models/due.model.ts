export interface Due {
    id?: number;
    amount: number;
    date: string;
    takenBy: string;
    promisedPaymentDate:string;
    note: string;
    customerId: number;
    interestRate: number;
    
}
