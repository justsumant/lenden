export interface Collection {
    id?: number;
    submittedBy: string;
    date: string;
    dueId: number;
    customerId:number;
    note:string;
    amount:number;
    interestRate: number;
}
