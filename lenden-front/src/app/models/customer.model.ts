export interface Customer {
    id?: number;
    name: string;
    address: string;
    contact: number;
    note: string;
    referredBy: string;
    guardian: string;
    
}