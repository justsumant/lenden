import { Component, EventEmitter, Input, OnInit, Output, ViewChild, OnChanges } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { ToastService } from 'src/app/dialog/toast/toast-service';
import { Collection } from 'src/app/models/collection.model';
import { CollectionsService } from 'src/app/services/collections.service';
import { DuesService } from 'src/app/services/dues.service';

@Component({
  selector: 'app-add-collection-modal',
  templateUrl: './add-collection-modal.component.html',
  styleUrls: ['./add-collection-modal.component.scss']
})
export class AddCollectionModalComponent implements OnChanges, OnInit {
  @ViewChild('closeButton') closeButton: any;
  @Input() selectedCustomer: any;
  @Output() collectionAddedEvent = new EventEmitter<number>();
  name: string = '';
  today = new Date();
  dues: any;
  startingDate = this.today.getFullYear() + '/' + (this.today.getMonth() + 1) + '/' + this.today.getDate();
  addCollectionForm:any;
  constructor(
    private dueService: DuesService,
    private collectionService: CollectionsService,
    private toastService: ToastService) {

  }

  ngOnInit(){
    this.addCollectionForm=new FormGroup({
      submittedBy: new FormControl(null, [Validators.required, Validators.minLength(4)]),
      startingDate: new FormControl(null, Validators.required),
      associatedTo: new FormControl(null),
      remarks: new FormControl(null),
      amount: new FormControl(null, Validators.required),
      rate: new FormControl(0, Validators.required)
    })
  }

  ngOnChanges(): void {
    this.getDuesofCustomer(this.selectedCustomer.id);
    
  }

/**
 * gets dues of selected customer for collection 
*/
  getDuesofCustomer(customerId: number) {
    this.dueService.getDuesOfCustomer(customerId).subscribe(dues => {
      this.dues = dues;
    }, err => {
      console.log(err);
    });
  }

  /**
   * adds a new collection
  */
  addCollection() {
    let amount = this.addCollectionForm.value.amount;
    let date = this.addCollectionForm.value.startingDate;
    let dueId = this.addCollectionForm.value.associatedTo;
    let note = this.addCollectionForm.value.remarks;
    let submittedBy = this.addCollectionForm.value.submittedBy;
    let interestRate = this.addCollectionForm.value.rate;

    let collection: Collection = {
      amount: amount,
      customerId: this.selectedCustomer.id,
      date: date,
      dueId: dueId,
      interestRate: interestRate,
      note: note,
      submittedBy: submittedBy
    }

    this.collectionService.addCollection(collection).subscribe(result => {
      this.toastService.show("Collection Added", { classname: 'lenden-primary text-light', delay: 5000 });
      // emits an event for customer detail page
      this.collectionAddedEvent.emit(this.selectedCustomer.id);
      this.addCollectionForm.reset();
    }, err => {
      console.log(err);

    })

    // close the modal programatically
    this.closeButton.nativeElement.click();
  }

}
