import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { ToastService } from 'src/app/dialog/toast/toast-service';
import { Customer } from 'src/app/models/customer.model';
import { CustomerService } from 'src/app/services/customer.service';

@Component({
  selector: 'app-add-customer-modal',
  templateUrl: './add-customer-modal.component.html',
  styleUrls: ['./add-customer-modal.component.scss']
})
export class AddCustomerModalComponent implements OnInit {
  @ViewChild('closeButton') closeButton: any;
  @Output() customerAddedEvent = new EventEmitter();

  name: string = '';
  today=new Date();
  startingDate = this.today.getFullYear()+'/'+(this.today.getMonth()+1)+'/'+this.today.getDate();
  addCustomerForm:any;
  constructor(private customerService: CustomerService, private toastService: ToastService) { }
//^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$


  ngOnInit(): void {
    this.addCustomerForm= new FormGroup({
      "name": new FormControl(null, [Validators.required, Validators.minLength(4)]),
      "guardian": new FormControl(null, [ Validators.minLength(4)]),
      "address": new FormControl(null, [Validators.required, Validators.minLength(4)]),
      "contact":new FormControl(null, [ Validators.pattern(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/)]),
      "referredBy":new FormControl(null, Validators.minLength(4)),
      "note":new FormControl(null)
    });
  }

  /**
   * adds new customer
  */
  addCustomer(){
    let name = this.addCustomerForm.value.name;
    let address = this.addCustomerForm.value.address;   
    let contact = this.addCustomerForm.value.contact; 
    let note = this.addCustomerForm.value.note;       
    let referredBy = this.addCustomerForm.value.referredBy;  
    let guardian = this.addCustomerForm.value.guardian;

    let newCustomer:Customer ={
      name: name,
      address: address,
      contact: contact,
      note: note,
      referredBy: referredBy,
      guardian: guardian
    }
    this.customerService.addNewCustomer(newCustomer).subscribe(result=>{
      this.toastService.show("Customer Created", { classname: 'lenden-primary text-light', delay: 5000 });
      this.customerAddedEvent.emit('true');
      
    }, err=>{
      console.log(err,'something wrong happened at server.....');
      
    });
    this.addCustomerForm.reset();
    // close the modal programmatically
    this.closeButton.nativeElement.click();

    
  }

}
