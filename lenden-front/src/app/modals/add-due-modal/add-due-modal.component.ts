import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { ToastService } from 'src/app/dialog/toast/toast-service';
import { Due } from 'src/app/models/due.model';
import { DuesService } from 'src/app/services/dues.service';

@Component({
  selector: 'app-add-due-modal',
  templateUrl: './add-due-modal.component.html',
  styleUrls: ['./add-due-modal.component.scss']
})
export class AddDueModalComponent implements OnInit {
  @ViewChild('closeButton') closeButton: any;
  @Input() selectedCustomer: any;
  @Output() dueAddedEvent = new EventEmitter<number>();
  name: string = '';
  today = new Date();
  startingDate = this.today.getFullYear() + '/' + (this.today.getMonth() + 1) + '/' + this.today.getDate();
  submittedBy: string = '';
  remarks: string = '';
  addDueForm: any;

  constructor(private dueService: DuesService, private toastService: ToastService) { }

  ngOnInit(): void {
    this.addDueForm=new FormGroup({
      amount: new FormControl(null, Validators.required),
      startingDate: new FormControl(null, Validators.required),
      takenBy: new FormControl('self', Validators.required),
      endsBy: new FormControl(null, Validators.required),
      remarks: new FormControl(null),
      rate: new FormControl(0, Validators.required)
    })
  }

  /**
   * Adds new Due
  */
  addDue() {
    let amount = this.addDueForm.value.amount;
    let remarks = this.addDueForm.value.remarks;
    let date = this.addDueForm.value.startingDate;
    let takenBy = this.addDueForm.value.takenBy;
    let completionDate = this.addDueForm.value.endsBy;
    let interestRate = this.addDueForm.value.rate;

    let due: Due = {
      amount: amount,
      date: date,
      note: remarks,
      takenBy: takenBy,
      promisedPaymentDate: completionDate,
      interestRate: interestRate,
      customerId: this.selectedCustomer.id
    }
    this.dueService.addDue(due).subscribe(result => {
      this.toastService.show("Due Added", { classname: 'lenden-primary text-light', delay: 5000 });
      // emits new event for customer details page
      this.dueAddedEvent.emit(this.selectedCustomer.id);
    }, err => {
      console.log(err);

    })

    this.addDueForm.reset();
    // close modal programmatically
    this.closeButton.nativeElement.click();


  }

}
