import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDueModalComponent } from './add-due-modal.component';

describe('AddDueModalComponent', () => {
  let component: AddDueModalComponent;
  let fixture: ComponentFixture<AddDueModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddDueModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDueModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
