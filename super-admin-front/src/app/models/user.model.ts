export interface User {
    id?: number;
    name: string;
    shopName: string;
    startingDate: string;
    endsBy: string;
    
}