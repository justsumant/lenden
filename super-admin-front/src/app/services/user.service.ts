import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  baseURL: string = 'http://localhost:3000';
  today = new Date().getFullYear() + '-' + new Date().getMonth() + '-' + new Date().getDate();


  constructor(private http: HttpClient) { }

  // getHeader() {
  //   return ({
  //     headers: {
  //       Authorization: 'Bearer ' + localStorage.getItem('token')
  //     }
  //   });
  // }

  addUser(user: User) {
    return this.http.post(this.baseURL + '/addUser', user);
  }

  deleteUser(id: number) {
    return this.http.get(this.baseURL + '/removeUser' + id);
  }

  getUsers() {
    return this.http.get(this.baseURL + '/usersList');
  }

  updateUser(user: User) {
    console.log(user);

    return this.http.post(this.baseURL + '/updateUser', user)
  }
}
