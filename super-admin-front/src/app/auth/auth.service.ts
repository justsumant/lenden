import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
baseURL: string = 'http://localhost:3000';


  constructor(private router: Router, private http: HttpClient) { 
    console.log('inside auth service and state is ', localStorage.getItem('token'));
    
  }

  login(userName: String, password: String){
    let user = {userName: userName, password: password};
    this.http.post(this.baseURL+'/auth/login', user).subscribe((data:any)=>{
      console.log(data);
      if(data.token){
        localStorage.setItem('token',data.token);
        localStorage.setItem('userId',data.loadedUserId);
        this.router.navigate(['/home/dashboard']);
      }
    
    }, error=>{
      console.log(error);
      
    });


    // this.isLoggedIn=true;
    // localStorage.setItem('isLoggedIn',this.isLoggedIn.toString());
  }
}
