import { Component } from "@angular/core";
import { NgForm } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthService } from "./auth.service";

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html'
})

export class AuthComponent{
userName: string='';
password: string='';
constructor(private authService:AuthService, private router: Router){
    // if(localStorage.getItem('token')){
    //     router.navigate(['/home/dashboard']);
    // }

}
login(form: NgForm){
console.log(form);
this.authService.login(form.value.username, form.value.password);
form.reset();
this.router.navigate(['/home/dashboard']);


}
    }