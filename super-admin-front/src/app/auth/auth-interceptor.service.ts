import { HttpEventType, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import {tap} from "rxjs/operators";

export class authInterceptor implements HttpInterceptor{
    intercept(req: HttpRequest<any>, next: HttpHandler){

        console.log(req.url);
        let modifiedRequest = req.clone({
            headers: req.headers.append('Authorization','Bearer ' + localStorage.getItem('token'))
        })


        // return next.handle(modifiedRequest).pipe(
        //     tap((event:any)=>{
        //         if(event.type = HttpEventType.Response){
        //             console.log(event.body);
                    
        //         }
        //     })
        // )
        return next.handle(modifiedRequest);
    }
}