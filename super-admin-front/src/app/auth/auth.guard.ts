import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { AuthService } from "./auth.service";

@Injectable({providedIn: "root"})
export class AuthGuard implements CanActivate{
    constructor(private authService: AuthService, private router:Router){

    }
    canActivate(route: ActivatedRouteSnapshot, router: RouterStateSnapshot): boolean | Promise<boolean> | Observable<boolean>{
        console.log('inside authguard sending', localStorage.getItem('token'));
        if(localStorage.getItem('token')!=undefined){
            return true;
        }else{
            this.router.navigate(['/auth']);
            return false;
        }
        
    }
}

//todo if token available, dont goto login page, 
// before opening dashboard, check for token validity, (backend - return error message on failure)
// if valid go to dashboard else clear token and take to login page