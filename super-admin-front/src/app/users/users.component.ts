import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { ConfirmationDialogService } from '../dialog/confirmation-dialog/confirmation-dialog.service';
import { User } from '../models/user.model';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users: any;
  editUserFlag: boolean = false;
  editableUser: any;
  constructor(
    private userService: UserService,
    private router: Router,
    private confirmationDialogueService: ConfirmationDialogService) {
    this.getUsers();
  }

  ngOnInit(): void {
  }

  getUsers() {
    this.userService.getUsers().subscribe(data => {
      console.log(data);
      this.users = data;
    }, err => {
      if (err.status == 401) {
        // user is unauthenticated, direct to login page.. 
        this.router.navigate(['/auth']);
      } else {
        console.log('something wrong happened with the server.... ', err);
      }

    });
  }


  deleteUser(id: number) {
    this.confirmationDialogueService.confirm('Confirm Delete', 'This is going to permanently delete the customer which is irreversible.').then(result => {
      console.log(result);
      if (result) {
        this.userService.deleteUser(id).subscribe(result => {
          console.log('user deleted successfully...', result);
          this.getUsers();
        }, err => {
          console.log('error while deleting the user...', err);

        });
      }

    }).catch(err => {
      console.log(err, 'confimation dialogue closed by close button.');

    });

  }

  selectUserToEdit(user: any) {
    console.log(user);
    this.editUserFlag = true;
    this.editableUser = user;

  }

  editUser(editUserForm: any) {
    console.log(editUserForm);
    let user: User = {
      name: editUserForm.value.name,
      shopName: editUserForm.value.shopName,
      startingDate: editUserForm.value.startingDate,
      endsBy: editUserForm.value.endsBy,
      id: editUserForm.value.id
    }
    this.userService.updateUser(user).subscribe(result=>{
      console.log(result);
      this.editUserFlag=false;
      
    }, err=>{
      console.log(err);
      
    })
  }

}
