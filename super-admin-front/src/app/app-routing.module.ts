import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { AuthGuard } from './auth/auth.guard';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';
import { UsersComponent } from './users/users.component';

const routes: Routes = [
  {path:'', redirectTo:'/auth', pathMatch: 'full'},
  {path:'auth', component: AuthComponent },
  {path:'home', canActivate:[AuthGuard], component: HomeComponent,
  children:[
    { path:'users', component: UsersComponent },
    { path:'dashboard', component: DashboardComponent }
  ]
  },
  {path:'*', redirectTo:'/auth'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
