import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {
  @ViewChild('closeButton') closeButton: any;
  name: string = '';
  today=new Date();
  startingDate = this.today.getFullYear()+'/'+(this.today.getMonth()+1)+'/'+this.today.getDate();
  constructor(private userService: UserService) { }

  ngOnInit(): void {
    
  }

  addUser(form: NgForm){
    console.log(form);
    let name = form.value.name;
    let shopName = form.value.shopName;   
    let startingDate = form.value.startingDate; 
    let endsBy = form.value.endsBy;            
    console.log(name,shopName, startingDate, endsBy);
    let newUser:User ={
      name: name,
      shopName: shopName,
      startingDate: startingDate,
      endsBy: endsBy
    }
    this.userService.addUser(newUser).subscribe(result=>{
      console.log(result,'user added successfully...');
      
    }, err=>{
      console.log(err,'something wrong happened at server.....');
      
    });
    form.reset();
    this.closeButton.nativeElement.click();

    
  }

}
