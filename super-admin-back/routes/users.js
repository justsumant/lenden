const path = require('path');
const express = require('express');
const userController = require('../controllers/userController');
const isAuth = require('../middleware/isAuth');
const router = express.Router();

//customers routes
router.get('/usersList', isAuth, userController.getUsers);
router.post('/addUser', isAuth, userController.addUser);
router.get('/removeUser:userId', isAuth, userController.removeUser);
router.post('/updateUser', isAuth, userController.editUser);




module.exports = router;