const path = require('path');
const express = require('express');
const authController = require('../controllers/authController');
const router = express.Router();

// authenticated routes, the route passes from left to right sequence.
router.post('/login', authController.login);




module.exports = router;