const Sequelize = require('sequelize');
const sequelize = require('../utils/database');

const User = sequelize.define('user', {
    id:{
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    shopName: Sequelize.STRING,
    startingDate: Sequelize.DATEONLY,
    endsBy: Sequelize.DATEONLY


});

module.exports = User;