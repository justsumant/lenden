const AdminUser = require('../models/adminUser');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

/**
 * login the admin
*/
exports.login = (req, res, next) => {
    console.log('inside login route...', req);
    const email = req.body.userName;
    const password = req.body.password;
    let loadedUser;
    AdminUser.findOne({where:{email: email}}).then(user => {
        if(!user){
            const error = new Error('User cannot be found with given email ID.');
            error.statusCode = 401;
            throw(error);
        }
        loadedUser = user;
        bcrypt.compare(password, user.password)
        .then(doMatch=>{
            if(!doMatch){
                const error = new Error('Wrong Password');
                error.statusCode = 401;
                throw(error);
            }
            const token = jwt.sign(
                {email: loadedUser.email, id: loadedUser.id}, 
                'someverySecuredSecretTextHere', 
                {expiresIn: '1h'});
                res.status(200).json({token: token, loadedUserId: loadedUser.id.toString()})
                
            
        }).catch(err=>{
            if(err.statusCode){
                err.statusCode=500;
            }
            console.log(err);
            res.status(401).json(err);
            
        });
        
    }).catch(err => {
        console.log(err);
        res.status(401).json(err);
    });
};
