const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const db = require('./utils/database');
const userRoutes = require('./routes/users');
const adminRoutes = require('./routes/admin');
const bcrypt = require('bcryptjs');

const User = require('./models/user');
const AdminUser = require('./models/adminUser');


app.use(bodyParser.json());

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT, PATCH');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
});

app.use('', userRoutes);
app.use('/auth', adminRoutes);



db.sync(
    // { force: true }
).then(result => {
    // console.log(result);

    User.create({
        name: 'user name',
        shopName: 'shop name',
        startingDate: new Date(),
        endsBy: new Date()
    });

    // adding additional logic, this will help while adding signup functionality which is not in our currennt project scope
    AdminUser.findOne({ where: { email: 'guptasumant517@gmail.com' } })
        .then(user => {
            if (user) {
                console.log('guptasumant517@gmail.com already exists, skipping creation of new user.');
                return;
            }
            bcrypt.hash('justsumant', 12).then(hashedPassword => {
                AdminUser.create({
                    name: 'sumant gupta',
                    email: 'guptasumant517@gmail.com',
                    password: hashedPassword
                });
                return;
            });

        });



}).then(adminUserResult => {
    console.log('1 admin user created...');
}).then(userResult => {
    console.log('1 user created...');
    // app.listen(3000);
}).catch(err => {

    console.log(err);
}).finally(() => {
    app.listen(3000);
});

