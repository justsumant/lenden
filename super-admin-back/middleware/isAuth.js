const jwt = require('jsonwebtoken');



module.exports = (req,res,next)=>{
    // check if the user is logged in or has a valid token or not
    // if not, retrun from the if condition
    

    if(!req.get('Authorization')){
        const error = new Error('Not Authenticated');
        error.statusCode = 401;
        throw error;
    }
    const token = req.get('Authorization').split(' ')[1];
    try {
        decodedToken = jwt.verify(token,'someverySecuredSecretTextHere');
        
    } catch (error) {
        console.log('error from token verification function...', error);
        // if(error == jwt.TokenExpiredError){
        //     console.log('token expired ');
        //     error.statusCode = 401;
        //     throw error;
        // }
        error.statusCode = 401;
        throw error;
    }

    if(!decodedToken){
        const error = new Error('Not Autheticated');
        error.statusCode = 401;
        throw error;
    }
    req.userId = decodedToken.userId;
    next();

}

